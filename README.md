# Mocha Base App
This application provides the basic scaffolding for an independent Mocha test suite.

### Getting started
1. Make sure you have [npm](https://www.npmjs.com/) installed.
2. Run `$ npm install` to install local dependencies (you might need to `sudo` this)
3. If you don't have `mocha` installed globally, run `$ npm install -g mocha`.
4. Once everything is installed, run `$ mocha app.js`. It should report that one test passed, and one test failed.
