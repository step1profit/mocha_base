
// TEST LIBS
var expect = require("chai").expect;
var should = require('chai').should();
var assert = require('chai').assert;
var request = require("request");


// TEST VARS
// move to config file
var quandl_token = 'BFo1MU4JSFRCWeCFDHyQ';
// bad token from 'Simple Example'
// https://www.quandl.com/help/api
var quandl_token_bad = 'dsahFHUiewjjd';
var quandl_api_url = 'https://www.quandl.com/api/v1/';


// TEST FUNCTIONS
function call_url(str, token, params){
  x = quandl_api_url + str + '?auth_token=' + token;
  if (params) {
    return x + params;
  } else {
    return x;
  }
}

/**
* RANDOM STRING GENERATOR
*
* Info:      http://stackoverflow.com/a/27872144/383904
* Use:       randomString(length [,"A"] [,"N"] );
* Default:   return a random alpha-numeric string
* Arguments: If you use the optional "A", "N" flags:
*            "A" (Alpha flag)   return random a-Z string
*            "N" (Numeric flag) return random 0-9 string
*/
function randomString(len, an){
  an = an&&an.toLowerCase();
  var str="", i=0, min=an=="a"?10:0, max=an=="n"?10:62;
  for(;i++<len;){
    var r = Math.random()*(max-min)+min <<0;
    str += String.fromCharCode(r+=r>9?r<36?55:61:48);
  }
  return str;
}



//
// AUTH TOKEN
// "You will need an authentication token unless you are doing fewer than 50 calls per day"
// https://www.quandl.com/help/api#Auth-Tokens
//
// https://www.quandl.com/help/api#Getting-Favourites
describe("GET Favorites using good auth token", function() {
  var res = {};
  var e;
  var url = call_url('current_user/collections/datasets/favourites.json', quandl_token);
  var x;

  before(function(done) {
    request.get(url, function(e, response) {
      err = e;
      res = response;
      done();
    });
  });

  it("should be success 200", function() {
    expect(res.statusCode).to.equal(200);
  });

  it("should return body as string with chars", function() {
    expect(res.body).to.be.a('string');
    expect(res.body).to.not.be.empty;
  });

  it("should have parsable json in body string", function() {
    should.not.Throw(function(){
      x = JSON.parse(res.body);
    });
  });

  it("should have js object in parsed json in body string", function() {
    x.should.be.an('object');
  });

  it("should have specific keys for feature", function() {
    var y = ["total_count","current_page","per_page","docs"];

    y.forEach(function(z) {
      x.should.have.property(z);
    });

    // or this
    x.should.have.all.keys(y);
  });
});

describe("GET Favorites using bad auth token", function() {
  var res = {};
  var err;
  var url = call_url('current_user/collections/datasets/favourites.json', quandl_token_bad);
  var x;

  before(function(done) {
    request.get(url, function(e, response) {
      err = e;
      res = response;
      done();
    });
  });

  it("should be status code: 401 unauthorized", function() {
    expect(res.statusCode).to.equal(401);
  });

  it("should return body as string with chars", function() {
    expect(res.body).to.be.a('string');
    expect(res.body).to.not.be.empty;
  });

  it("should have parsable json in body string", function() {
    should.not.Throw(function(){
      x = JSON.parse(res.body);
    });
  });

  it("should have js object in parsed json in body string", function() {
    x.should.be.an('object');
  });

  it("should have error key with val: 401 unauthorized", function() {
    x.should.have.property('error').equal('401 Unauthorized');
  });
});



//
// SEARCH
//
describe("GET Search: awesome; using good auth token", function() {
  this.timeout(30000);

  var res = {};
  var err;
  var url = call_url('datasets.json', quandl_token, '&query=awesome');
  var x;

  before(function(done) {
    request.get(url, function(e, response) {
      err = e;
      res = response;
      done();
    });
  });

  it("should be 200 success", function() {
    expect(res.statusCode).to.equal(200);
  });

  it("should return body as string with chars", function() {
    expect(res.body).to.be.a('string');
    expect(res.body).to.not.be.empty;
  });

  it("should have parsable json in body string", function() {
    should.not.Throw(function(){
      x = JSON.parse(res.body);
    });
  });

  it("should have js object in parsed json in body string", function() {
    x.should.be.an('object');
  });

  it("should have total_count above 500; meaning query has 500 or more results", function() {
    x.should.have.property('total_count');
    x.total_count.should.be.above(500);
  });
});

describe("GET Search: [random string]; using good auth token", function() {
  this.timeout(30000);

  var res = {};
  var err;
  var rand_str = randomString(20);
  var url = call_url('datasets.json', quandl_token, '&query=' + rand_str);
  var x;

  before(function(done) {
    request.get(url, function(e, response) {
      err = e;
      res = response;
      done();
    });
  });

  it("should be 200 success", function() {
    expect(res.statusCode).to.equal(200);
  });

  it("should return body as string with chars", function() {
    expect(res.body).to.be.a('string');
    expect(res.body).to.not.be.empty;
  });

  it("should have parsable json in body string", function() {
    should.not.Throw(function(){
      x = JSON.parse(res.body);
    });
  });

  it("should have js object in parsed json in body string", function() {
    x.should.be.an('object');
  });

  it("should have total_count with 0; meaning query has 0 results", function() {
    x.should.have.property('total_count').equal(0);
  });
});



//
// DATA SETS
// https://www.quandl.com/help/api#Quandl-Codes
//
describe("GET Dataset: AWESOME INVESTMENTS INC.; using good auth token", function() {
  var res = {};
  var err;
  var url = call_url('datasets/GOOG/FRA_A2W.json', quandl_token);
  var x;

  before(function(done) {
    request.get(url, function(e, response) {
      err = e;
      res = response;
      done();
    });
  });

  it("should be 200 success", function() {
    expect(res.statusCode).to.equal(200);
  });

  it("should return body as string with chars", function() {
    expect(res.body).to.be.a('string');
    expect(res.body).to.not.be.empty;
  });

  it("should have parsable json in body string", function() {
    should.not.Throw(function(){
      x = JSON.parse(res.body);
    });
  });

  it("should have js object in parsed json in body string", function() {
    x.should.be.an('object');
  });

  it("should have dataset keys in parsed body json string", function() {
    // keys that identify object as dataset
    y = [
     "code",
     "column_names",
     "data",
     "description",
     "display_url",
     "errors",
     "frequency",
     "from_date",
     "id",
     "name",
     "premium",
     "private",
     "source_code",
     "source_name",
     "to_date",
     "type",
     "updated_at",
     "urlize_name"
    ]
    x.should.have.all.keys(y);
  });
});

describe("GET Dataset: AWESOME INVESTMENTS INC.; Specific Columns: Date; using good auth token", function() {
  var res = {};
  var err;
  var url = call_url('datasets/GOOG/FRA_A2W.json', quandl_token, '&column=0');
  var x;

  before(function(done) {
    request.get(url, function(e, response) {
      err = e;
      res = response;
      done();
    });
  });

  it("should be 200 success", function() {
    expect(res.statusCode).to.equal(200);
  });

  it("should return body as string with chars", function() {
    expect(res.body).to.be.a('string');
    expect(res.body).to.not.be.empty;
  });

  it("should have parsable json in body string", function() {
    should.not.Throw(function(){
      x = JSON.parse(res.body);
    });
  });

  it("should have js object in parsed json in body string", function() {
    x.should.be.an('object');
  });

  it("should have single date column in column_names val", function() {
    assert.lengthOf(x.column_names, 1);
    x.column_names.should.be.instanceof(Array);
    x.column_names.should.include('Date');
  });
});


//
// Bad URLs
//
describe("GET datasets no params; using good auth token", function() {
  this.timeout(40000);

  var res = {};
  var err;
  var url = call_url('datasets.json', quandl_token);
  var x;

  before(function(done) {
    request.get(url, function(e, response) {
      err = e;
      res = response;
      done();
    });
  });

  it("should be status code: 200 success", function() {
    expect(res.statusCode).to.equal(200);
  });

  it("should return body as string with chars", function() {
    expect(res.body).to.be.a('string');
    expect(res.body).to.not.be.empty;
  });

  it("should have parsable json in body string", function() {
    should.not.Throw(function(){
      x = JSON.parse(res.body);
    });
  });

  it("should have js object in parsed json in body string", function() {
    x.should.be.an('object');
  });

  // 18885425 is current total_count as of 20150410112738
  it("should have total_count above 18885424; meaning query has 18885424 or more results--with no params api returns ALL records as default", function() {
    x.should.have.property('total_count');
    x.total_count.should.be.above(18885424);
  });
});

describe("GET datasets bad params; using good auth token", function() {
  this.timeout(40000);

  var res = {};
  var err;
  var rand_prm = '&' + randomString(20) + '=' + randomString(20);
  var url = call_url('datasets.json', quandl_token, rand_prm);
  var x;

  before(function(done) {
    request.get(url, function(e, response) {
      err = e;
      res = response;
      done();
    });
  });

  it("should be status code: 200 success", function() {
    expect(res.statusCode).to.equal(200);
  });

  it("should return body as string with chars", function() {
    expect(res.body).to.be.a('string');
    expect(res.body).to.not.be.empty;
  });

  it("should have parsable json in body string", function() {
    should.not.Throw(function(){
      x = JSON.parse(res.body);
    });
  });

  it("should have js object in parsed json in body string", function() {
    x.should.be.an('object');
  });

  // 18885425 is current total_count as of 20150410112738
  it("should have total_count above 18885424; meaning query has 18885424 or more results--api disregards bad params and returns ALL records as default", function() {
    x.should.have.property('total_count');
    x.total_count.should.be.above(18885424);
  });
});

describe("GET bad dataset; using good auth token", function() {
  var res = {};
  var err;
  var rand_str = randomString(20);
  var url = call_url('datasets/' + rand_str + '.json', quandl_token);
  var x;

  before(function(done) {
    request.get(url, function(e, response) {
      err = e;
      res = response;
      done();
    });
  });

  it("should be status code: 404 not found", function() {
    expect(res.statusCode).to.equal(404);
  });

  it("should return body as string with chars", function() {
    expect(res.body).to.be.a('string');
    expect(res.body).to.not.be.empty;
  });

  it("should have parsable json in body string", function() {
    should.not.Throw(function(){
      x = JSON.parse(res.body);
    });
  });

  it("should have js object in parsed json in body string", function() {
    x.should.be.an('object');
  });

  it("should have error key with val: requested entity does not exist", function() {
    x.should.have.property('error').equal('Requested entity does not exist.');
  });
});

describe("GET bad url; using good auth token", function() {
  var res = {};
  var err;
  var rand_str = randomString(20);
  var url = call_url(rand_str + '.json', quandl_token);
  var x;

  before(function(done) {
    request.get(url, function(e, response) {
      err = e;
      res = response;
      done();
    });
  });

  it("should be status code: 404 not found", function() {
    expect(res.statusCode).to.equal(404);
  });

  it("should return body as string with chars", function() {
    expect(res.body).to.be.a('string');
    expect(res.body).to.not.be.empty;
  });

  it("should have parsable json in body string", function() {
    should.not.Throw(function(){
      x = JSON.parse(res.body);
    });
  });

  it("should have js object in parsed json in body string", function() {
    x.should.be.an('object');
  });

  it("should have error key with val: unknown api route", function() {
    x.should.have.property('error').equal('Unknown api route.');
  });
});

describe("GET bad url; set headers application/json; using good auth token", function() {
  var res = {};
  var err;
  var rand_str = randomString(20);
  var url = call_url(rand_str, quandl_token);
  var x;
  var opts = {
    url: url,
    headers: {
      "Content-Type": "application/json" 
    }
  }

  before(function(done) {
    request.get(opts, function(e, response) {
      err = e;
      res = response;
      done();
    });
  });

  it("should be status code: 404 not found", function() {
    expect(res.statusCode).to.equal(404);
  });

  it("should return body as string with chars", function() {
    expect(res.body).to.be.a('string');
    expect(res.body).to.not.be.empty;
  });

  it("should have parsable json in body string", function() {
    should.not.Throw(function(){
      x = JSON.parse(res.body);
    });
  });

  it("should have js object in parsed json in body string", function() {
    x.should.be.an('object');
  });

  it("should have error key with val: unknown api route", function() {
    x.should.have.property('error').equal('Unknown api route.');
  });
});
